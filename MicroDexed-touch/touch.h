#ifndef _TOUCH_H
#define _TOUCH_H

#if defined GENERIC_DISPLAY
#include "XPT2046_Touchscreen.h"
extern XPT2046_Touchscreen touch;
#endif

#ifdef CAPACITIVE_TOUCH_DISPLAY
#include "Adafruit_FT6206.h"
extern Adafruit_FT6206 touch;
#endif

#include <SD.h>

typedef struct dexed_live_mod_s
{
  uint8_t active_button = 0;
  uint8_t orig_attack_values[2][7];
  uint8_t orig_release_values[2][7];

#if NUM_DEXED > 1
  int attack_mod[NUM_DEXED] = { 0, 0 };
  int release_mod[NUM_DEXED] = { 0, 0 };
#else
  int attack_mod[NUM_DEXED] = { 0 };
  int release_mod[NUM_DEXED] = { 0 };
#endif

} dexed_live_mod_t;

typedef struct ts_s
{
  uint8_t virtual_keyboard_octave = 3;
  uint8_t virtual_keyboard_instrument = VK_DEXED0;
  uint8_t virtual_keyboard_prev_instrument = 0xFF;
  uint8_t virtual_keyboard_midi_channel = 1;
  uint8_t virtual_keyboard_velocity = 120;
  uint8_t msp_peak[NUM_MULTISAMPLES];
  uint8_t current_virtual_keyboard_display_mode = 0;   // 0=keys 1=pads 
  uint8_t previous_virtual_keyboard_display_mode = 99; // 0=keys 1=pads 

  TS_Point p;

  uint32_t virtual_keyboard_state_white = 0;
  uint32_t virtual_keyboard_state_black = 0;

  uint8_t displayed_peak[20]; // volmeter peak levels, currently displayed level
  uint8_t old_helptext_length[3];
  bool touch_ui_drawn_in_menu = false;
  bool keyb_in_menu_activated = false;
  uint8_t fav_buttton_state = 0;
} ts_t;

// (Touch)File Manager

typedef struct fm_s
{
  uint8_t wav_recorder_mode = 0;

  uint8_t sample_source = 0; // 0 = SD, 1 = UNUSED, 2 = MSP1, 3 = MSP2, 4 = PSRAM
  int sample_screen_position_x = 0;
  uint8_t active_window = 0; // 0 = left window (SDCARD) , 1 = PSRAM
  uint16_t sd_sum_files = 0;
  uint8_t sd_cap_rows;
  uint8_t sd_folder_depth = 0;
  uint16_t sd_selected_file = 0;
  uint16_t sd_skip_files = 0;
  uint8_t sd_mode = 0;
  bool sd_is_folder;
  bool sd_parent_folder = false;
  String sd_new_name;
  String sd_full_name;
  String sd_prev_dir;
  String sd_temp_name;

  uint16_t psram_sum_files = 0;
  uint16_t psram_cap_rows;
  uint16_t psram_selected_file = 0;
  uint16_t psram_skip_files = 0;
} fm_t;

static constexpr int TOUCH_MAX_REFRESH_RATE_MS = 10; // 100Hz
typedef void (*TouchFn)();

void registerTouchHandler(TouchFn touchFn);
void unregisterTouchHandler(void);
TouchFn getCurrentTouchHandler(void);
void updateTouchScreen();


void handle_touchscreen_braids(void);
void handle_touchscreen_arpeggio();
void handle_touchscreen_midi_channel_page();
void handle_touchscreen_mixer();
void handle_touchscreen_microsynth();
void handle_touchscreen_pattern_editor();
void handle_touchscreen_voice_select();
void handle_touchscreen_multiband();
void handle_touchscreen_menu();
void handle_touchscreen_test_touchscreen();
void handle_touchscreen_sample_editor();
void handle_touchscreen_settings_button_test();
void handle_touchscreen_liveseq_pianoroll();
void handle_touchscreen_liveseq_listeditor();
void handle_touchscreen_file_manager();
void handle_touchscreen_mute_matrix();
void handle_touchscreen_custom_mappings();
void handle_touchscreen_drums();
//void handle_touchscreen_drums();

#endif
